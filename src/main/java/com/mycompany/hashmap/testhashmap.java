/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.hashmap;
import java.util.ArrayList;


/**
 *
 * @author Kitty
 */
public class testhashmap {
    public static void main(String[] args) {
        
        hashmap n1 = new hashmap(100,"Tanadon"); //add values 
        n1.put(501, "Horakul");
        n1.put(124, "bad");
        
         //get values from key
        System.out.println(n1.get(501));
        System.out.println(n1.get(100));
        System.out.println(n1.get(124));
        
        System.out.println("\nremove values at key: 100"); //remove values
        System.out.println(n1.remove(100));
        System.out.println(n1.get(100));
        
    }
}
